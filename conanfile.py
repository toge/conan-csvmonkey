from conans import ConanFile

class CsvmonkeyConan(ConanFile):
    name           = "csvmonkey"
    version        = "0.0.5"
    license        = "BSD-3-Clause"
    author         = "toge.mail@gmail.com"
    url            = "https://bitbucket.org/toge/conan-csvmonkey/"
    homepage       = "https://github.com/dw/csvmonkey"
    description    = "Header-only vectorized, lazy-decoding, zero-copy CSV file parser "
    topics         = ("csv parser", "header only", "vectorized")
    generators     = "cmake"
    no_copy_source = True

    def source(self):
        self.run("git clone https://github.com/dw/csvmonkey/")
        self.run("cd csvmonkey && git checkout 594ad9854b7a77ccb8e26260b1e1a4300ba18465")

    def package(self):
        self.copy("*.hpp", dst="include", src="csvmonkey/include")

    def package_info(self):
        self.info.header_only()